import { Component } from '@angular/core';
import { FeedService } from './services/feed.service';

@Component({
  selector: 'my-app',
  template: `
    <header></header>
    <sidebar></sidebar>
    <feed></feed>
  `,
  styles: [`

    sidebar {
      float:left;
      width: 25%;
      height:100%
    }

    feed {
      float:left;
      width: 75%;
      height:100%;
    }

  `]
})
export class AppComponent  {
  constructor(private feedService: FeedService) {
    this.feedService.read();
  }
}

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs';

import * as X2JS from 'x2js';


@Injectable()
export class FeedService {

  private cache: Object;
  private feed$: BehaviorSubject<Object>;
  feed: Observable<Object>;

  constructor(private http: Http) {
    this.feed$ = new BehaviorSubject({});
    this.feed = this.feed$.asObservable();
  }

  read() {
    let parser = new X2JS({
      ignoreRoot: true,
      arrayAccessForm: 'property'
    });

    this.http.get('http://stage.whitehatgaming.com/progtest/games.xml')
      .subscribe((res: Response) => {
        let data = parser.xml2js(res.text());
        data.categories = ['All Games'];

        for(let game of data.gametype) {
          let category = game.name.toString();
          data.categories.push(category);
        }

        this.cache = data;
        this.feed$.next(data);
      });
  }

  filterCategory(id: string) {

    let data: any = this.cache;
    let next: any = Object.assign({}, data);

    if(id === 'All Games') {
      this.feed$.next(data);
      return;
    }

    for(let category of data.gametype ) {
      if (category.name.toString() === id) {
        next.gametype = [category];
        this.feed$.next(next);
        break;
      }
    }
  }

}

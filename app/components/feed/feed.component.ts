import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FeedService } from '../../services/feed.service';

@Component({
  selector: 'feed',
  template: `
    <div *ngFor="let category of categorisedGames" class="games">
      <h2> {{category.name.toString()}} </h2>
      <div *ngFor="let game of category.item_asArray" (click)="showLink(game._g_link)" class="game">
        <h3> {{game._gname}} </h3>
        <img [src]="game._g_img">
      </div>
    </div>
  `,
  styles: [`
    .game {
      display: inline-block;
      width: 30%;
      margin-bottom: 10px;
      padding-right: 10px;
      cursor: pointer;
    }
  `]
})
export class FeedComponent implements OnInit, OnDestroy  {

  private subscription: Subscription;

  categorisedGames: Object;

  constructor(public feedService: FeedService) {}

  ngOnInit() {
    this.subscription = this.feedService.feed.subscribe((data: any) => {
      this.categorisedGames = data.gametype;
    })
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  showLink(link: string) {
    alert(link);
  }
}

import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { FeedService } from '../../services/feed.service';

@Component({
  selector: 'sidebar',
  template: `
    <div>
      <h2> Browse categories </h2>
      <ul>
        <li *ngFor="let category of categories" (click)="this.feedService.filterCategory(category)"> {{category}} </li>
      </ul>
    </div>
  `,
  styles: [`

    :host {
      display: block;
    }

    ul {
      width: 100%;
      margin: 0;
      padding: 0;
      overflow: hidden;
      list-style: none;
    }

    li {
      font-size: 18px;
      font-weight: bold;
      cursor: pointer;
    }

  `]
})
export class SidebarComponent  {

  private subscription: Subscription;

  categories: Array<string>;

  constructor(public feedService: FeedService) {}

  ngOnInit() {
    this.subscription = this.feedService.feed.subscribe((data: any) => {
      this.categories = data.categories;
    })
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
